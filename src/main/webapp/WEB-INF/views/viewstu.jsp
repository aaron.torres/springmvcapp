<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tabla de resultados</title>
</head>
<body> 
  
<h1>Tabla de resultados</h1>  
<table border="2" width="70%" cellpadding="2">  
<tr><th>Id</th><th>Name</th><th>Age</th><th>Edit</th><th>Delete</th></tr>  
   <c:forEach var="stu" items="${list}">   
   <tr>  
   <td>${stu.id}</td>  
   <td>${stu.name}</td>  
   <td>${stu.age}</td>  
   <td><a href="editstu/${stu.id}">Edit</a></td>  
   <td><a href="deletestu/${stu.id}">Delete</a></td> 
   </tr>  
   </c:forEach>  
   </table>  
   <br/>  
   <a href="person">Add New Student</a>
</body>
</html>