package com.softtek.academy.springmvc.dao;

import java.sql.ResultSet;    
import java.sql.SQLException;    
import java.util.List;    
import org.springframework.jdbc.core.BeanPropertyRowMapper;    
import org.springframework.jdbc.core.JdbcTemplate;    
import org.springframework.jdbc.core.RowMapper;

import com.softtek.academy.springmvc.beans.Person;

public class StudentDao {
	JdbcTemplate template;    
    
	public void setTemplate(JdbcTemplate template) {    
	    this.template = template;    
	}    
	public int save(Person p){    
	    String sql="insert into student(id,name,age) values("+p.getId()+",'"+p.getName()+"',"+p.getAge()+")";    
	    return template.update(sql);    
	}    
	
	public int update(Person p){    
	    String sql="update student set name='"+p.getName()+"', age="+p.getAge()+" where id="+p.getId()+"";    
	    return template.update(sql);    
	}    
	public int delete(int id){    
	    String sql="delete from student where id="+id+"";    
	    return template.update(sql);    
	}    
	public Person getPersonById(int id){    
	    String sql="select * from student where id=?";    
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Person>(Person.class));    
	}    
	
	public List<Person> getStudents(){    
	    return template.query("select * from student",new RowMapper<Person>(){    
	        public Person mapRow(ResultSet rs, int row) throws SQLException {    
	        	Person e=new Person();    
	            e.setId(rs.getInt(1));    
	            e.setName(rs.getString(2));    
	            e.setAge(rs.getInt(3));       
	            return e;    
	        }    
	    });    
	}    
}
