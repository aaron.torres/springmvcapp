package com.softtek.academy.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.springmvc.beans.Person;
import com.softtek.academy.springmvc.dao.StudentDao;

@Controller
public class PersonController {
	
	@Autowired
	StudentDao dao;
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
	}
	
	/*@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("SpringMVCApp")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        return "newPerson";
    }*/
	 @RequestMapping(value="/save",method = RequestMethod.POST)    
	    public String save(@ModelAttribute("SpringMVCApp") Person person){    
        dao.save(person);    
        return "redirect:/viewstu";  
     }  
	 
	 @RequestMapping("/viewstu")    
	    public String viewstu(Model m){    
        List<Person> list=dao.getStudents();    
        m.addAttribute("list",list);  
        return "viewstu";    
     }   
	 @RequestMapping(value="/editstu/{id}")    
	    public String edit(@PathVariable int id, Model m){    
        Person person=dao.getPersonById(id);    
        m.addAttribute("command",person);  
        return "stueditform";    
	    }    
	    /* It updates model object. */    
	    @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("SpringMVCApp") Person person){    
        dao.update(person);    
        return "redirect:/viewstu";    
	    }    
	    /* It deletes record for the given id in URL and redirects to /viewemp */    
	    @RequestMapping(value="/deletestu/{id}",method = RequestMethod.GET)    
	    public String delete(@PathVariable int id){    
        dao.delete(id);    
        return "redirect:/viewstu";    
	    }     
}

